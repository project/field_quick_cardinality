<?php

namespace Drupal\field_quick_cardinality;

use Drupal\Core\Entity\EntityInterface;
use Drupal\field_ui\FieldConfigListBuilder as FieldUIFieldConfigListBuilder;

/**
 * Extended class of the default FieldConfigListBuilder.
 */
class FieldConfigListBuilder extends FieldUIFieldConfigListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $build = parent::buildHeader();
    $this->injectBeforeLastElement($build, 'cardinality', $this->t('Cardinality'));
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $field_config) {
    $row = parent::buildRow($field_config);
    $value = ($field_config->getFieldStorageDefinition()->get("cardinality") > 0) ? $field_config->getFieldStorageDefinition()->get("cardinality") : $this->t("Unlimited");
    $this->injectBeforeLastElement($row['data'], "cardinality", $value);
    return $row;
  }

  /**
   * Injects an extra column just before the last column of the table.
   *
   * @param array $table
   *   The array of the table keyed by the column names.
   * @param string $key
   *   The key to use for the new column.
   * @param string $value
   *   The value of the column.
   */
  protected function injectBeforeLastElement(array &$table, $key, $value) {
    $last_key = array_key_last($table);
    $last_value = $table[$last_key];
    unset($table[$last_key]);
    $table[$key] = $value;
    $table[$last_key] = $last_value;
  }

}
